import redis
import time
import logging
import datetime
r = redis.Redis(host='127.0.0.1', port=6379, db=0)
LOG_FILENAME='syserror.txt'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)

def calculationTime(parameter,currentTime):
    currentTimeForWrong = (currentTime - datetime.timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S")
    parameter = parameter.decode("utf-8")

    if parameter == currentTimeForWrong:
        return logging.error(currentTimeForWrong+' Trend time is outdated')

while True:
    currentTime=datetime.datetime.now()
    if int(currentTime.second) % 15 == 0 :
        parameter = r.get('trend_update_time')
        calculationTime(parameter,currentTime)
    time.sleep(1)
