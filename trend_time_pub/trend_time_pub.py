import time
import datetime
import redis

counter=1
r = redis.Redis(host='127.0.0.1', port=6379, db=0)


def calculationTime(counter):
    currentTime = datetime.datetime.now()

    currentTimeForFifth = datetime.datetime.now() - datetime.timedelta(hours=1)
    if counter % 2 != 0:
        calcTime=currentTime.strftime("%Y-%m-%d %H:%M:%S")
        return calcTime
    else:
        calcTime2=currentTimeForFifth.strftime("%Y-%m-%d %H:%M:%S")
        return calcTime2

while True:
    if int(datetime.datetime.now().second) % 30 == 0:
        r.set('trend_update_time',calculationTime(counter))
        counter = counter + 1
    time.sleep(1)
